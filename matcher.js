var dal = require('./dal');
var geoConfig = require('config').get('distances');
var logger = require('./logger');

module.exports = function(order){
  //Distance for requested country
  var distance = (order.country && geoConfig[order.country]) || geoConfig.default;

  logger.info('Find cleaners by location longitude =', order.longitude, 'latitude =', order.latitude, 'distance =', distance);

  return dal.findCleanersByLocation(order.longitude, order.latitude, distance)
  .then(function(cleaners){
    logger.info('Found', cleaners.length, 'cleaners')
    //Filter by gender if gender specified
    if(order.gender){
      logger.info('Filter cleaners by gender:', order.gender);
      cleaners = cleaners.filter(function(cleaner){
        return cleaner.gender === order.gender;
      });
    }

    //Filter by preferences if preferences specified
    if(order.preferences){
      logger.info('Filter cleaners by preferences:', order.preferences);
      cleaners = cleaners.filter(function(cleaner){
        var orderPreferences = order.preferences.split(',');

        //Check that all cleaner's preferences satisfy requested preferences
        return orderPreferences.every(function(p){
          return cleaner.preferences.indexOf(p) !== -1;
        });
      });
    }

    return cleaners;
  })
  .catch(function(err){
    logger.error('Failed to find cleaners', err);
    return [];
  })
};
