var Sequelize = require('sequelize');
var config = require('config').get('psql');
var logger = require('./logger');

logger.info('Connecting to database', config.database, 'at', config.host);

var db = new Sequelize(config.database, config.user, config.password, {
  host: config.host,
  dialect: 'postgres',
  logging: false
});

//Cleaner model
var Cleaner = db.define('cleaner', {
 id: {type: Sequelize.STRING(20), primaryKey: true},
 name: {type: Sequelize.STRING(255)},
 country_code: {type: Sequelize.STRING(2)},
 latitude: {type: Sequelize.FLOAT},
 longitude: {type: Sequelize.FLOAT},
 gender: {type: Sequelize.STRING(1)},
 preferences: {type: Sequelize.STRING(255)}
}, {tableName: 'cleaners', timestamps: false});

exports.findCleanersByLocation = function(longitude, latitude, distance){
  return db.query('SELECT * FROM cleaners WHERE (point(?, ?) <@> point(longitude, latitude)) < ?',{
    replacements: [longitude, latitude, toMiles(distance)],
    type: db.QueryTypes.SELECT
  });
};

exports.setup = function(){
  return db.sync()
  .then(function(){
    return db.query('CREATE EXTENSION IF NOT EXISTS cube', {raw: true});
  })
  .then(function(){
    return db.query('CREATE EXTENSION IF NOT EXISTS earthdistance', {raw: true});
  })
  .catch(function(err){
    logger.error('Failed to setup DAL', err);
  });
};

exports.close = function(){
  return db.close();
};

exports.insert = function(data){
  return Cleaner.bulkCreate(data);
};

function toMiles(km){
  return km * 0.62137273665;
}
