# BOOK A TIGER Exercise #

Cleaners matching cli tool.

### Installation ###

To install dependencies please run the following command:

```
#!bash

npm install
```

Application configuration files are located in ./config directory. Please specify your PostgreSQL user and password in default.json.

To create tables in PostgreSQL and install required extensions please run: 

```
#!bash

gulp setup
```

To restore dump containing test data please run the following command:
```
#!bash

gulp restore-dump
```

### Usage ###

Example of get-matches command:
```
#!bash

gulp get-matches --country=de --geo=52.5126466,13.4154251  --preferences=windows,oven --gender=M
```