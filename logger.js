/**
 * Winston logger configuration
 */
var winston = require('winston');
var config = require('config').get('logger')

var options = {
	transports: [new winston.transports.Console({
		level: config.level
	})]
};

var logger = new (winston.Logger)(options);

module.exports = logger;
