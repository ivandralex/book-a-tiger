var logger = require('./logger');
var findMatchingCleaners = require('./matcher');
var dal = require('./dal');
var dataConfig = require('config').get('data')
var minimist = require('minimist');
var util = require('util')

var OUTPUT_FORMAT = '- id: %s - name: %s - gender: %s - preferences:%s'
var NOT_FOUND = 'No matches found.';

var gulp = require('gulp')

gulp.task('get-matches', function (cb) {
  var order = require('minimist')(process.argv.slice(2));

  logger.debug('Order', order, typeof order.geo);

  if(typeof order.geo != 'string'){
    logger.error('Error: geo point is not specified');
    cb();
    return;
  }
  else{
    var coordinates = order.geo.split(',')
    order.latitude = Number(coordinates[0]);
    order.longitude = Number(coordinates[1]);
  }

  findMatchingCleaners(order)
  .then(function(cleaners){
    if(cleaners.length == 0){
      logger.info(NOT_FOUND);
    }
    else{
      cleaners.forEach(function(cleaner){
        var str = util.format(OUTPUT_FORMAT, cleaner.id, cleaner.name, cleaner.gender, cleaner.preferences);
        logger.info(str);
      });
    }

    return dal.close();
  })
  .then(function(){
    cb();
  })
  .catch(function(err){
    logger.error('Failed to find cleaners', err);
  });
});

//This task creates table in Postgres and installs required extensions
gulp.task('setup', function (cb) {
  dal.setup()
  .then(function(){
    return dal.close();
  })
  .then(function(){
    cb();
  })
  .catch(function(err){
    logger.error('Failed to setup DAL', err);
  })
});

//This task adds test cleaners to db
gulp.task('restore-dump', function (cb) {
  dal.insert(dataConfig.dump)
  .then(function(){
    return dal.close();
  })
  .then(function(){
    cb();
  })
  .catch(function(err){
    logger.error('Failed to populate db with data', err);
  });
});

process.on('uncaughtException', function (err) {
  logger.error('Uncaught exception', err);
});
